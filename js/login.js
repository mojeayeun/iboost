/**
 * @author mojeayeun
 *  good -- http://www.9lessons.info/2014/07/ajax-php-login-page.html
 */


$(document).ready(function(){


	//default로 표시
	$("#username").attr("value",localStorage.re_uid);
	$("#password").attr("value",localStorage.re_pwd);

	//validation check
	//submit으로 하면 오류발생함 (http://cricri4289.blogspot.kr/2013/10/jquery-ajax-error-code-0.html)
	//$(form).submit(function(){
	$("#login").click(function() {
		var lid,pwd;


		if(!$("#username").attr("value")){
			alert("enter username");
			$("#username").focus();
			return false;
		}else{
			lid = $("#username").val();
		}
		
		if(!$("#password").attr("value")){
			alert("enter password");
			$("#password").focus();
			return false;
		}else{
			pwd = $("#password").val();
		}
		
		$.ajax({
			url: "ajaxLogin.php",
			type: "POST",
			data :{
				"username"    : lid,
				"password"	: pwd
			},
			cache: false,
			beforeSend:function(){
				$("#add_err").css('display', 'inline', 'important');
				$("#add_err").html("<img src='../images/ajax-loader.gif' /> login...please wait a minute");
	   		},
			success: function(ret_val){

			console.log("login:"+ret_val);
			console.dir(ret_val);

   				if(ret_val == 1){

					//login성공시 localstorage에 id/pwd저장
					localStorage.re_uid = lid;
					localStorage.re_pwd = pwd;

					window.location="main.php";

				}else{
					$("#add_err").css('display', 'inline', 'important');
					$("#add_err").css('font-size','small');
					$("#add_err").html("<img src='../images/alert.png' />username or password is wrong.");
				}
			   
				
			},
			
			error: function(xhr, message, errorThrown){
				var msg = xhr.status + " / " + message + " / " + errorThrown;
				console.dir(xhr); 
				alert(msg);
				 
			}
		});
		
		return false;
	});
		

});
