<?php
session_start();
//session check
if(empty($_SESSION['username'])) 
{
	header('Location: index.php');
}else{
	$username= $_SESSION['username'];    //user name
}
?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
	<!--<meta name="apple-mobile-web-app-capable" content="yes">-->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<meta name="author" content="mo jea yeun">
	<title>Main Page</title>

	<link rel="apple-touch-icon" href="../images/app_icon.png" />
	<link rel="apple-touch-icon-precomposed" href="../images/app_icon.png" />
	<link rel="apple-touch-startup-image" href="../images/startup.png"/>
	 
	<link href="../css/common.css" rel="stylesheet" type="text/css">


	 <script>
			window.addEventListener('load', function(){setTimeout(scrollTo, 0, 0, 1);}, false);
	 </script>

	 <script src="../js/jquery-1.8.3.min.js"></script>
	 <script src="../js/common.js"></script> <!--common-->
	   
	<!--[if lt IE 9]> 
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->


</head>
 
<body>

	<div class="header_div">
	 <span class="uname">Welcome &nbsp;<?=$_SESSION['username']?></span>
	</div><!-- t2_userinfo -->

	<div>
	<div>
		<iframe class="doc_div" src="<?=$_SESSION['doc']?>" width="100%" height="500" frameborder="0" marginheight="0" marginwidth="0">loading...</iframe>
	</div>><!-- doc_div -->
	
</body>
</html>
