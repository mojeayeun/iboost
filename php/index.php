<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi" />
	<!--<meta name="apple-mobile-web-app-capable" content="yes">-->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<meta name="author" content="mo jea yeun">
	<title>Login</title>

	<link rel="apple-touch-icon" href="../images/app_icon.png" />
	<link rel="apple-touch-icon-precomposed" href="../images/app_icon.png" />
	<link rel="apple-touch-startup-image" href="../images/startup.png"/>
	 
	<link rel="stylesheet" href="../css/style.css">
	<!--<link rel="stylesheet" href="css/new_style.css">-->

	 <script>
			window.addEventListener('load', function(){setTimeout(scrollTo, 0, 0, 1);}, false);
	 </script>

	
	 <script src="../js/jquery-1.8.3.min.js"></script>
	 <script src="../js/common.js"></script> <!--request-->
	 <script src="../js/login.js"></script> <!--request-->
	   
	<!--[if lt IE 9]> 
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->


	</head>

	<body>
 

 <form action="../index.html" class="login">
	<div id="add_err"></div>
    <h1><span class="loginlogo"></span></h1>
    <input type="text" id="username" name="username" class="login-input" placeholder="username" value="AirEleven" autofocus>
    <input type="password" id="password" name="password" class="login-input" value="aireleven12#" placeholder="password">
    <input type="submit" value="Login" class="login-submit" id="login">
  </form>

	
	  </body>
	</html>
